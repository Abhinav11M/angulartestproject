System.register(['angular2/platform/browser', 'angular2/core', './CountDownComponent'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var browser_1, core_1, CountDownComponent_1;
    var PomodoroTimerComponent;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (CountDownComponent_1_1) {
                CountDownComponent_1 = CountDownComponent_1_1;
            }],
        execute: function() {
            PomodoroTimerComponent = (function () {
                function PomodoroTimerComponent() {
                    var _this = this;
                    this.isPaused = true;
                    this.buttonLabel = 'Resume';
                    this.resetPomodoro();
                    setInterval(function () { return _this.tick(); }, 1000); //Run this.tick every 1000 milliseconds
                }
                PomodoroTimerComponent.prototype.resetPomodoro = function () {
                    this.minutes = 24;
                    this.seconds = 59;
                };
                PomodoroTimerComponent.prototype.tick = function () {
                    if (!this.isPaused) {
                        if (--this.seconds < 0) {
                            this.seconds = 59;
                            --this.minutes;
                        }
                        if (this.minutes < 0) {
                            this.resetPomodoro();
                        }
                    }
                };
                PomodoroTimerComponent.prototype.getSeconds = function () {
                    return this.seconds;
                };
                PomodoroTimerComponent.prototype.togglePause = function () {
                    this.isPaused = !this.isPaused;
                    this.buttonLabel = this.isPaused ? 'Resume' : 'Pause';
                };
                PomodoroTimerComponent.prototype.OnCountDownCompleted = function () {
                    //alert("Time's Up!!!!");
                    console.log("Time's Up!!!!");
                };
                PomodoroTimerComponent = __decorate([
                    core_1.Component({
                        selector: 'pomodoro-timer',
                        directives: [CountDownComponent_1.CountdownComponent],
                        template: "\n    <h1> {{ minutes }}:{{ seconds }} </h1>\n            <p>\n                <button (click)=\"togglePause()\">\n                {{ buttonLabel }}\n                </button> \n            </p>\n            <div class=\"container text-center\">\n              <countdown [seconds]=\"25\"\n                (complete)=\"OnCountDownCompleted()\">\n                #counter            \n              </countdown>\n              <p>\n                <button class=\"btn btn-default\" (click)=\"counter.seconds = 25\">Reset Countdown to 25\n                </button>\n              </p>\n              <p *ngIf=\"counter.seconds < 10\">\n                Beware! Only <strong> {{counter.seconds}} seconds</strong> left.\n              </p>\n            </div>        \n  "
                    }), 
                    __metadata('design:paramtypes', [])
                ], PomodoroTimerComponent);
                return PomodoroTimerComponent;
            }());
            browser_1.bootstrap(PomodoroTimerComponent);
        }
    }
});
//# sourceMappingURL=pomodoro-timer.js.map