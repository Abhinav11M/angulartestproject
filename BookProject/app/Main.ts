import { bootstrap } from 'angular2/platform/browser';
import {Component} from 'angular2/core';
import { PomodoroTimerComponent } from './pomodoro-timer';

@Component({
    selector: 'timer',
    directives : [PomodoroTimerComponent],
    template: `
        <countdown></countdown>
    `
})

/**
 * MainClass
 */
class MainClass {
}

bootstrap(MainClass);