import { Component, Input, Output, EventEmitter } from 'angular2/core';

@Component({
  selector:'countdown',
  template: `
    <h1>Time Left : {{seconds}}</h1>
  `
})

export /***
 * CountdownComponent
 */
class CountdownComponent {
  @Input() seconds: number;
  intervalId :number;
  @Output() complete: EventEmitter<any> = new EventEmitter();
  @Output() progress: EventEmitter<number> = new EventEmitter();

  constructor() {
    this.intervalId = setInterval(() => this.tick(), 1000);//Setinterval's firsr argument is a lambda and 2nd is the time
  }

  tick(): void {
    this.progress.emit(this.seconds);//Passing this.seconds to event emitted
    if(--this.seconds < 1){
      clearInterval(this.intervalId);//Clearing setIInterval using setInterval ID
      this.complete.emit(null);//Emitting an event once the timer reaches 0
    }
  }
}