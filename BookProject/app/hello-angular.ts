import { bootstrap } from 'angular2/platform/browser';
import { Component } from 'angular2/core';

@Component({
    selector: 'hello-angular',
    // template: `
    //     <h1>Welcome to PeoplePortal</h1>
    //     <h2>{{pageTitle}}</h2>
    // `
    template: '<h1> {{greeting}} </h1>'
})

export /**
 * HelloAngularComponent
 */
class HelloAngularComponent {
   //pageTitle: string = 'PeoplePortal'
   greeting: string;
   constructor() {
       this.greeting = 'Hello Angular 2!';
  }
}

//bootstrap(HelloAngularComponent);