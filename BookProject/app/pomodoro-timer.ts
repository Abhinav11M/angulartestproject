import { bootstrap } from 'angular2/platform/browser';
import { Component } from 'angular2/core';
import { CountdownComponent } from './CountDownComponent';

@Component({
  selector: 'pomodoro-timer',
  directives: [CountdownComponent],
  template: `
    <h1> {{ minutes }}:{{ seconds }} </h1>
            <p>
                <button (click)="togglePause()">
                {{ buttonLabel }}
                </button> 
            </p>
            <div class="container text-center">
              <countdown [seconds]="25"
                (complete)="OnCountDownCompleted()">
                #counter            
              </countdown>
              <p>
                <button class="btn btn-default" (click)="counter.seconds = 25">Reset Countdown to 25
                </button>
              </p>
              <p *ngIf="counter.seconds < 10">
                Beware! Only <strong> {{counter.seconds}} seconds</strong> left.
              </p>
            </div>        
  `
  // template: `<h1> {{ minutes }}:{{ seconds }} </h1>
  //           <p>
  //               <button (click)="togglePause()">
  //               {{ buttonLabel }}
  //               </button> 
  //           </p>
  //           <div class="container text-center">
  //             <countdown [seconds]="25"
  //               (progress)="timeout = $event"
  //               (complete)="OnCountDownCompleted()">            
  //             </countdown>
  //             <p *ngIf="timeout < 10">
  //               Beware! Only <strong> {{timeout}} seconds</strong> left.
  //             </p>
  //           </div>        
  //`
})
//Here the value of seconds is getting passed to the input parameter seconds of CountdownComponent class.
//(complete)="OnCountDownCompleted()"-- Here we are subscribing to complete when the event is emitted.
class PomodoroTimerComponent {
  minutes: number;
  seconds: number;
  isPaused: boolean = true;
  buttonLabel: string = 'Resume';
  timeout: number;

  constructor() {
    this.resetPomodoro();
    setInterval(() => this.tick(), 1000);//Run this.tick every 1000 milliseconds
  }

    resetPomodoro(): void{
        this.minutes = 24;
        this.seconds = 59;
    }

    private tick(): void{
        if(!this.isPaused){
          if(--this.seconds < 0){
              this.seconds = 59;
              --this.minutes;
          }
          if(this.minutes < 0){
              this.resetPomodoro();
          }
        }
    }

    getSeconds(): any{
      return this.seconds;
    }

    togglePause(): void{
      this.isPaused = !this.isPaused;
      this.buttonLabel = this.isPaused ? 'Resume' : 'Pause';
    }

    OnCountDownCompleted(): void{
      //alert("Time's Up!!!!");
      console.log("Time's Up!!!!");
    }
  }

bootstrap(PomodoroTimerComponent);